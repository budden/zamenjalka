"""Похоже, что устарела"""

import json
import sys
from pygments import lex
from pygments.token import Token
from pygments.lexers import get_lexer_by_name
import pprint
import re
import jsonpatch_z as jsonpatch

from з_типы_данных import лКласс, лТекст, гДети, гТип, гТекст, гЛексема, гКлюч
import свойства_групп
import группировка_лексем
import вывод_групп

def СравниВерховныеГруппы(лСгф,пСгф,имяЛексера,доУровня):
    assert(имяЛексера=='OCaml') # иное не умеем
    словарьЛ = вывод_групп.ВыведиГруппыПодГжельДоУровня(лСгф,лСгф.верховнаяГруппа,доУровня)
    with open('джл.json','w') as джлф:
        print(json.dumps(словарьЛ,ensure_ascii=False,indent=1),file=джлф)

    словарьП = вывод_групп.ВыведиГруппыПодГжельДоУровня(пСгф,пСгф.верховнаяГруппа,доУровня)
    with open('джп.json','w') as джпф:
        print(json.dumps(словарьП,ensure_ascii=False,indent=1),file=джпф)


    print("=" * 10 + "  ИЗМЕНЕНИЯ ПУТЕЙ " + "=" * 50)
    патчСодержимого = jsonpatch.JsonPatch.from_diff(словарьЛ, словарьП)
    print(json.dumps(патчСодержимого.patch,ensure_ascii=False,indent=1))

    return патчСодержимого


def СериализуйЭлтДиффаВJson(obj):
    if isinstance(obj, type):
        return str(obj)
    raise TypeError ("СериализуйЭлтДиффаВJson: %s is not serializable" % type(obj))



