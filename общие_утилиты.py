import ruamel.yaml as yaml
import json, sys, io, re
from pathlib import Path
from pygments import token
import tempfile
import subprocess

def СравниДвеСтрокиПриПомощиMeld(string1, string2):
    # Создание временных файлов
    with tempfile.NamedTemporaryFile(mode='w', delete=False) as file1, \
            tempfile.NamedTemporaryFile(mode='w', delete=False) as file2:
        # Запись строк во временные файлы
        file1.write(string1)
        file2.write(string2)

    # Вызов команды meld для сравнения файлов
    subprocess.run(['meld', file1.name, file2.name])

    # Удаление временных файлов
    file1.close()
    file2.close()

def represent_tuple(dumper, data):
    return dumper.represent_sequence('tag:yaml.org,2002:seq', data)

yaml.add_representer(tuple, represent_tuple)

#def represent_token(dumper, data):
#    return dumper.represent_sequence('tag:yaml.org,2002:seq', [type(data).__name__])

yaml.add_representer(tuple, represent_tuple)
# yaml.add_representer(token._TokenType, represent_token)

def ИзмВЗам(начЗнач):
    return [начЗнач]

измВЗам = 0 # изменяется в замыкании
# выражение вида X[измВЗам] - это типа указателя

def YamlDump(x, stream=None, безЗавершающегоКС=True):
    # yamll=yaml.YAML(typ='unsafe',pure=True)
    if stream is not None:
        assert(not безЗавершающегоКС)
        return yaml.dump(x, default_flow_style=True,allow_unicode=True,stream=stream)
    else:
        р1 = yaml.dump(x, default_flow_style=True,allow_unicode=True,stream=stream)
        if безЗавершающегоКС:
            assert(р1[-1]=='\n')
            return р1[:-1]
        else:
            return р1

def JsonDump(x, stream=None):
    if stream is None:
        return json.dumps(x, indent=1,ensure_ascii=False)
    else:
        return json.dump(x, stream, indent=1,ensure_ascii=False)

def ПрочитайJsonОбъектИзФайла(иф):
    with open(иф, 'r') as file:
        data = json.load(file)
    return data


def ЗапишиJsonОбъект(о, кудаПисать):
    def ПишиВнутр(п):
        return json.dump(о, п, ensure_ascii=False, indent=1)
    if кудаПисать is None:
        return ПишиВнутр(sys.stdout)
    elif isinstance(кудаПисать, Path):
        with open(кудаПисать, 'w') as file:
            return ПишиВнутр(file)
    elif isinstance(кудаПисать, io.TextIOWrapper):
        ПишиВнутр(кудаПисать)
    else:
        assert ("Неведомый тип назначения, куда писать" == True)


def ЗапишиJsonОбъектСОтступом(о, кудаПисать, отступ):
    """Для вставки в код. Отступ = 4 - вставка в код первого уровня"""
    оКакСтрока=json.dumps(о, ensure_ascii=False, indent=4)
    indented_json = "\n".join("    " + line for line in оКакСтрока.splitlines())

    def ПишиВнутр(п):
        п.write(indented_json)
        п.flush()
    if кудаПисать is None:
        ПишиВнутр(sys.stdout)
    elif isinstance(кудаПисать, Path):
        with open(кудаПисать, 'w') as file:
            ПишиВнутр(file)
    elif isinstance(кудаПисать, io.TextIOWrapper):
        ПишиВнутр(кудаПисать)
    else:
        assert ("Неведомый тип назначения, куда писать" == True)



def КвоВхожденийСловаВСтроке(text, word):
    pattern = r'\b' + re.escape(word) + r'\b'
    matches = re.findall(pattern, text)
    return len(matches)


def ЗамениВхожденияСловаВПодстроке(text, word, replace_to):
    pattern = r'\b' + re.escape(word) + r'\b'
    return re.sub(pattern, replace_to, text)



def ТестПоискИЗаменаСлова():
    гдеИскать = "ab DabD ab ab e-f"
    assert (КвоВхожденийСловаВСтроке(гдеИскать, 'ab') == 3)
    assert (ЗамениВхожденияСловаВПодстроке(
        гдеИскать, 'ab', 'cd') == "cd DabD cd cd e-f")
    assert (КвоВхожденийСловаВСтроке(гдеИскать, 'e-f') == 1)
    assert (ЗамениВхожденияСловаВПодстроке(
        гдеИскать, 'e-f', 'g-h') == "ab DabD ab ab g-h")

ТестПоискИЗаменаСлова()