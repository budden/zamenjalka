
"""Утилиты для получения списков файлов, подлежащих обработке"""

import traceback
import subprocess
import os
import re
import io
import sys
import json
from pathlib import Path
import ruamel.yaml as yaml
from общие_утилиты import ИзмВЗам, измВЗам, YamlDump, JsonDump
from общие_утилиты import ЗапишиJsonОбъект, ПрочитайJsonОбъектИзФайла,  ЗапишиJsonОбъектСОтступом

def ЗапишиФайлыИзменённыеВГитеМеждуКоммитами(dir, commit1, commit2, иф):
    command = f'git diff --name-only {commit1} {commit2}'
    output = subprocess.check_output(command, shell=True, cwd=dir)
    changed_files = output.decode().splitlines()
    ЗапишиJsonОбъект(changed_files, иф)


def DirToJson(rootDir, masks, иф):
    file_paths = []
    for root, dirs, files in os.walk(rootDir):
        for file in files:
            for mask in masks:
                if file.endswith(mask):
                    file_path = os.path.join(root, file)
                    relative_path = os.path.relpath(file_path, rootDir)
                    file_paths.append(relative_path)
                    break
    ЗапишиJsonОбъект(file_paths, иф)
