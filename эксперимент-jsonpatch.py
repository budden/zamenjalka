#!/usr/bin/python3.7

import sys
import json
import jsonpatch_z as jsonpatch

def ГлавнаяСкрипта(argv):
    # ожидаем один аргумент - JSON
    if len(argv) != 2:
        print("Ожидает один параметр - номер теста")
        sys.exit(4)
    номерТеста = argv[1]
    if номерТеста == '1':
        Тест1()
    else:
        print("Неизвестный номер теста")


def Тест1():
    джл = {'b' : 'test СПО test'}
    джп = {'b' : 'test ОПО test'}
    патч = jsonpatch.JsonPatch.from_diff(джл, джп)

    патч2 = jsonpatch.JsonPatch.from_string("""
[
 {
  "op": "replace_text_in_value",
  "path": "/b",
  "from": "СПО",
  "to": "ОПО",
  "cnt": 2
 }
]""")

    рез1 = патч2.apply(джл)
    if джп == рез1:
        print("Ура")


if __name__ == "__main__":
    ГлавнаяСкрипта(sys.argv)
