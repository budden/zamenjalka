#!/usr/bin/python3.7

"""Дополнения к jsonpatch_z."""

import traceback
import subprocess
import os
import re
import io
import sys
import json
from pathlib import Path
import ruamel.yaml as yaml
from общие_утилиты import ИзмВЗам, измВЗам, YamlDump, JsonDump
from общие_утилиты import ЗапишиJsonОбъект, ПрочитайJsonОбъектИзФайла,  ЗапишиJsonОбъектСОтступом
import вывод_групп
import группировка_лексем
import свойства_групп
from з_типы_данных import лКласс, лТекст, гДети, гТип, гТекст, гЛексема, гКлюч
import jsonpatch_z as jsonpatch
from pygments.lexers import get_lexer_by_name
from pygments.token import Token
from pygments import lex
import typing
import unittest


def СоздайОперациюЗамены(искомое, замена, квоНаходок, чтоПолучилось, гдеИскали, ВЗначенииЛи):
    if ВЗначенииЛи is None:
        return {'op': 'replace_text_in_path', 'from': гдеИскали, 'path': чтоПолучилось,
                'cnt': квоНаходок, 'replace_from': искомое, 'replace_to': замена}
    else:
        return {'op': 'replace_text_in_value', 'path': ВЗначенииЛи,
                'from': гдеИскали, 'value': чтоПолучилось,
                'cnt': квоНаходок, 'replace_from': искомое, 'replace_to': замена}



def ВыкиньИзЗаплаткиЗначения(з):
    """Выкидвает значения, которые нужны только нам для изучения/отладки, 
    и не влияют на применимость заплатки к отличающемуся файлу"""
    if isinstance(з, dict):
        рез = dict()
        for k, v in з.items():
            if 'op' == 'replace_text_in_value':
                if k not in ('from', 'value'):
                    рез[k] = v
            else:
                рез[k] = v
    elif isinstance(з, list):
        рез = []
        for э in з:
            рез.append(ВыкиньИзЗаплаткиЗначения(э))
    else:
        assert ("Так не должно быть" == True)

    return рез


def ЗаплаткаИлиЧтоТоВJson(новаяЗаплатка):
    if isinstance(новаяЗаплатка, str):
        return новаяЗаплатка
    elif isinstance(новаяЗаплатка, jsonpatch.JsonPatch):
        return новаяЗаплатка.patch
    else:
        return type(новаяЗаплатка)


# Дзаплатка - сокращение от 'данные заплатки' - это чистые данные заплатки, которые можно превратить
# в JsonPatch. Кроме этого, преобразование пути из нашего формата в формат JsonPatch и обратно

# Зпуть - пусть по дереву, как он сделан в заплатке, в виде массива
# Зпутть - путь по дереву, как он сделан в заплатке, в виде текста
# Есть также ГКлюч, ГПуть - это путь в смысле вложенности иерархии групп. Они отличаются между собой. 

ТипЗпуть = typing.List
ТипЗпутть = typing.AnyStr

def УзелПоЗпутти(зПутть:ТипЗпутть, корень):
    """Дан зПутть (строковый путь из заплатки согласно RFC что-то там). Возвращаем 
    узел, ведущий к этому пути. Следи, чтобы узел и путь относились к одному и тому же этапу
    изменения документа. См. также свойства_групп.ДайПолныйЗпутьКГруппе"""
    jp = jsonpatch.JsonPointer(зПутть)
    узел = jp.resolve(корень)
    return узел

def ЗпутьВЗпутть(зпуть:ТипЗпуть):
    jp = jsonpatch.JsonPointer.from_parts(зпуть)
    рез = jp.path
    return рез

def ЗпуттьВЗпуть(зпуть:ТипЗпутть):
    jp = jsonpatch.JsonPointer(зпуть)
    рез = jp.parts.copy()
    return рез

def ПриведиЗКЗпутть(зпуттть):
    """На входе Зпуть или Зпутть. На выходе - Зпуть"""
    if isinstance(зпуттть, str):
        return зпуттть
    else:
        assert(isinstance(зпуттть, ТипЗпуть))
        рез=ЗпутьВЗпутть(зпуттть)
        return рез

def ПриведиЗКЗпуть(зпуттть):
    """На входе Зпуть или Зпутть. На выходе - Зпуть"""
    if isinstance(зпуттть, ТипЗпуть):
        return зпуттть
    else:
        ## нельзя проверить почему-то соответствие с Зпутть
        assert(isinstance(зпуттть, str))
        рез=ЗпуттьВЗпуть(зпуттть)
        return рез

def ОтносительныйЗпутьКЭтойГруппеИзРодительской(Сгф, г):
    р = свойства_групп.ДайРодителя(Сгф,г)
    дети = р[гДети]
    for i, д in enumerate(дети):
        if д == г:
            return ['д',str(i)]


def ОбработайВсеПутиВОперации(оп,фн):
    """Разрушительно меняет операцию, применяя к каждому пути в ней ф-ю фн"""
    def Обработай1(ключ):
        было = оп[ключ]
        будет = фн(было)
        оп[ключ] = будет
    т = оп['op']
    if т in ['replace','replace_text_in_path','remove','add']:
        Обработай1('path')
    elif т == 'move':
        Обработай1('from')
        Обработай1('path')
    elif т == 'print_and_substitute':
        Обработай1('path')
    else:
        assert("Неизвестная операция" == False)

def ДопишиПрефиксКоВсемПутямВОперации(оп,зпуть):
    def ДобавьЗпутьК(было):
        было = ПриведиЗКЗпуть(было)
        будет = зпуть + было
        return будет
    ОбработайВсеПутиВОперации(оп, ДобавьЗпутьК)

def ЗпутьВГпуть(сгф, Зпуттть):
    """Преобразует Зпут(т)ь в Гпуть"""
    Зпутть = ПриведиЗКЗпутть(Зпуттть)
    г = УзелПоЗпутти(Зпутть,сгф.верховнаяГруппа)
    гпуть = свойства_групп.ДайПолныйГпутьКГруппе(сгф, г)
    return гпуть

class Тесты(unittest.TestCase):
    # def test_смотримНаDiff(self):
    #     ж1 = [{'1':'a'}]
    #     ж2 = [{'1':'b'}]
    #     з = jsonpatch.JsonPatch.from_diff(ж1, ж2).patch
    #     JsonDump(з)

    def test_Зпуть(self):
        зпуть = ['1','a']
        зпутть = '/1/a'
        self.assertEqual(ЗпуттьВЗпуть(зпутть),зпуть)
        self.assertEqual(ЗпутьВЗпутть(зпуть),зпутть)
        self.assertEqual(ПриведиЗКЗпутть(зпуть),зпутть)
        self.assertEqual(ПриведиЗКЗпутть(зпутть),зпутть)
        self.assertEqual(ПриведиЗКЗпуть(зпуть),зпуть)
        self.assertEqual(ПриведиЗКЗпуть(зпутть),зпуть)

if __name__ == '__main__':
    unittest.main()

