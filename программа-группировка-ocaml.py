#!/usr/bin/python3.7

"""Частично устарела, оставляем для примера, потом надо обновить"""

import группировка_лексем
import свойства_групп
import вывод_групп
import сравнивалка
import замена_текста
import общие_утилиты
import sys
import json
import ruamel.yaml as yaml

def ГлавнаяСкрипта(argv):
    # ожидаем один аргумент - JSON
    if len(argv) != 2:
        ПишиСправкуИУмри()
    аргт = json.loads(argv[1])
    глагол = аргт.get('--глагол')
    if глагол == 'напечатай-группы':
        ПрочтиИРаспечатайГруппыOCaml(аргт)
    elif глагол == 'сравни-файлы-ocaml':
        СравниДваФайлаOCaml(аргт)
    else:
        ПишиСправкуИУмри()


def ПрочтиИРаспечатайГруппыOCaml(аргт):
    иф = аргт['--входной-файл']
    сгф = группировка_лексем.ПрочтиФайл(иф, 'OCaml')
    print("======== Группы: ========")
    текстВерховнойГруппы = свойства_групп.ЯвиТекстГруппыПлюс(сгф, сгф.верховнаяГруппа,выводитьГраницы=True)
    print(текстВерховнойГруппы)
    х = вывод_групп.ВыведиГруппыПодГжельДоУровня(сгф, сгф.верховнаяГруппа, 7)
    print("======== Под Гжель: ========")
    print(json.dumps(х,ensure_ascii=False,indent=1))

def СравниДваФайлаOCaml(аргт):
    """Генерирует хотя бы разницу между файлами"""
    лф = аргт['--левый-файл']
    пф = аргт['--правый-файл']
    имяЛексера = 'OCaml'
    лСгф = группировка_лексем.ПрочтиФайл(лф, имяЛексера)
    пСгф = группировка_лексем.ПрочтиФайл(пф, имяЛексера)
    сравнивалка.СравниВерховныеГруппы(лСгф,пСгф,имяЛексера,7)

def ПишиСправкуИУмри():
    print("""Принимает в качестве параметра JSON выражение, структуру см. в исходнике, 
    вот два варианта:
    {"--глагол": "напечатай-группы", "--входной-файл": "имяФайла"} 
    {"--глагол":"сравни-файлы-ocaml","--левый-файл имяЛевогоФайла": "--правый-файл имяПравогоФайла"}
    """,file=sys.stderr)
    sys.exit(1)

if __name__ == "__main__":
    ГлавнаяСкрипта(sys.argv)
