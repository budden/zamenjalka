#!/usr/bin/python3.7

"""
В оптимизированные_замены_подгрупп_2 выявляются замены текста. Теперь мы хотим их сделать более удобными:

* обобщить до менее специфичного пути (расширяем область, пока преобразование остаётся корректным)
* использовать ГПуть вместо ЗПути
"""

import traceback
import subprocess
import os, re, io, sys, json, math
from pathlib import Path
import ruamel.yaml as yaml
import общие_утилиты
import вывод_групп
import группировка_лексем
import свойства_групп
import поиск_замен_по_списку
from з_типы_данных import лКласс, лТекст, гДети, гТип, гТекст, гЛексема, гКлюч
import jsonpatch_z as jsonpatch
from pygments.lexers import get_lexer_by_name
from pygments.token import Token
from pygments import lex
import гит_и_директории
import доп_jsonpatch_z
import unittest
from copy import deepcopy
import неизвестная_замена_в_списке

def РасширьОбластьДействияОперацииPrintAndSubstitute(лСгф, оп):
    """Есть операция print_and_substitute. Расширяем её (сокращаем кво сегментов пути) до тех пор, пока она всё ещё работает.
    Полученную операцию уже нельзя использовать без того, чтобы после применения не распарсить текст заново. Хотя 
    мы могли бы и дерево тоже подменять. Или сделать это?""" 
    assert(оп['op'] == 'print_and_substitute')
    зпуть = доп_jsonpatch_z.ПриведиЗКЗпуть(оп['path'])
    чтоИскать = оп['from']
    наЧтоЗаменять = оп['value']
    заплатка = jsonpatch.JsonPatch([оп])
    верховнаяГруппа = лСгф.верховнаяГруппа
    результатПрименения = заплатка.apply(верховнаяГруппа)
    (_к,полныйТекстПослеПрименения) = свойства_групп.ЯвиТекстУпрощённойГруппы(результатПрименения)
    улучшеннаяОперация = оп
    while True:
        if len(зпуть) == 0:
            return улучшеннаяОперация
        зпуть = зпуть[:-2]
        улучшеннаяОп1 = deepcopy(улучшеннаяОперация)
        улучшеннаяОп1['path'] = доп_jsonpatch_z.ПриведиЗКЗпутть(зпуть)
        заплатка1 = jsonpatch.JsonPatch([улучшеннаяОп1])
        результатПрименения1 = заплатка1.apply(верховнаяГруппа)
        (_к1,полныйТекстПослеПрименения1) = свойства_групп.ЯвиТекстУпрощённойГруппы(результатПрименения1)
        if полныйТекстПослеПрименения1 != полныйТекстПослеПрименения:
            общие_утилиты.СравниДвеСтрокиПриПомощиMeld(полныйТекстПослеПрименения, полныйТекстПослеПрименения1)
            return улучшеннаяОперация
        улучшеннаяОперация = улучшеннаяОп1
        

